[![pipeline status](https://gitlab.com/neuro-oncologia/tac2/badges/master/pipeline.svg)](https://gitlab.com/oncologiaped/eventosadversos/commits/master)

# Caderno aberto de pesquisas em oncologia

## O que é:

Repositório de um caderno de pesquisas aberto em oncologia pediátrica.

## Objetivo:

Divulgar de forma transparente dados de projetos de pesquisa envolvendo crianças e adolescentes que fazem tratamento para câncer.

## Visão:

Melhorar a qualidade de vida de crianças e adolescentes com câncer, estimulando a pesquisa através da livre troca de dados.

## Missão:

Divulgar **todos** os dados de **todas** as pesquisas realizadas pelo nosso grupo.

## Como contribuir:

- Crie uma conta no [Gitlab][lab] (gratuita) e abra uma **questão** ([_issue_][issue]).
- **Duplique** ([_fork_][fork]) o repositório, faça alterações e **peça uma emenda** ([_merge request_][merge]).

## Manifesto:

O conceito de **caderno de pesquisa aberto** é relativamente recente, e
permanece com uma definição fluida. Em poucas palavras, trata-se da disponibilização
do conteúdo científico, _processado ou não_, incluindo _dados de pesquisa
coletados_, _dados de pesquisa categorizados, filtrados e/ou analisados_,
comentários, listas de tarefas, registros de reuniões, esboços de textos
acadêmicos, e outros materiais passíveis de armazenamento eletrônico, de forma
livre na rede mundial de computadores, geralmente com alguma licença de acesso
gratuito.
O termo **caderno de pesquisa aberto** foi introduzido pelo pesquisador
da Universidade de Drexel (Pensilvânia, EUA), [Jean Claude Bradley][bradley]. Químico, entusiasta
do movimento **open source** dos desenvolvedores de programas para computadores,
ele declarou que utilizaria o termo **open notebook science**, a fim de
diferenciar dos movimentos análogos, porém diferentes, das publicações de acesso
aberto (_open access_) e da **ciência aberta** (_open science_, na época ainda
denominada _open source science_). Bradley declarou:

>   "Nos programas de código aberto, o código é disponibilizado para
    qualquer pessoa modificar e utilizar para outros fins. O que
    estamos tentando fazer com [UsefulChem][useful]
    é prover uma entidade análoga para a pesquisa em química, ou seja, dados
    brutos de experimentação, juntamente com a interpretação do pesquisador,
    em um formato que qualquer um possa tomar para reanalisar, reinterpretar
    e reutilizar de outra forma. Um bom exemplo de reposicionamento de dados
    é o de utilizar resultados de experimentos que falharam de uma maneira
    diferente daquela planejada inicialmente. Isso nunca ocorre na
    ciência porque resultados mal sucedidos quase nunca são
    incluídos em publicações."

Este texto, retirado da postagem original do blog de Bradley, ilustra a proximidade
entre o conceito original de **caderno de pesquisa aberto** e o atual
movimento por maior transparência na divulgação de **todos** os
resultados de pesquisas clínicas. Segundo Ben Goldacre, médico psiquiatra fundador
da iniciativa [AllTrials.net][alltrials], cerca de metade dos ensaios
clínicos nunca reportou seus dados, por motivos diversos, levando à existência de um
grande conjunto dedados científicos clínicos que permanece <b>oculto</b> em bancos
de dados abandonados.
A possível existência de dados úteis para outras situações não previstas pelo
ensaio clínico original já seria justificativa suficiente para tornar estes dados
públicos. Porém, uma outra preocupação é a possibilidade da repetição de pesquisas
que já demonstraram ser mal sucedidas, muitas vezes usando fundos públicos de
recursos. Isso encarece a pesquisa clínica de uma forma geral e a torna menos
eficiente. Além disso, tratamentos que mostraram ser inseguros correm o risco de
serem testados novamente quando os dados originais de pesquisas anteriores não
são divulgados.
Por este motivo, decidi transformar meus arquivos eletrônicos de pesquisa
clínica, a maioria revisões de literatura e análises retrospectivas, em um caderno
de pesquisa aberto. Acredito que o investimento nesta vertente pode aumentar a
visibilidade da pesquisa clínica, diminuir as dúvidas de especialistas e leigos
e otimizar os custos com a pesquisa clínica de uma forma geral. Esta iniciativa está
aberta à comunidade acadêmica e clínica de uma forma em geral, para contribuições e
comentários.

[lab]: https://gitlab.com
[issue]: https://gitlab.com/neuro-oncologia/tac2/issues
[fork]: https://gitlab.com/neuro-oncologia/tac2/forks/new
[merge]: https://gitlab.com/dashboard/merge_requests?assignee_id=592949
[bradley]: https://en.wikipedia.org/wiki/Jean-Claude_Bradley
[useful]: http://usefulchem.blogspot.com.br
[alltrials]: http://alltrials.net
