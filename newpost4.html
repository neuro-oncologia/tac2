<h2 id="maistransparncianapesquisaclnica">Mais transparência na pesquisa clínica</h2>

<p>Os motivos pelos quais <a href="https://en.m.wikipedia.org/wiki/Clinical_research">pesquisas clínicas</a> são realizadas deveriam ser óbvios. No entanto, é preciso abordá-los e tentar resumi-los em alguns princípios de trabalho, se nos propusermos a uma análise aprofundada. Provavelmente, é um consenso que a pesquisa clínica visa desenvolver novos tratamentos para doenças. Dito isto, deve ser um corolário de que a pesquisa clínica busca o benefício do público. Além disso, é primordial na pesquisa clínica a proteção do sujeito experimental.
Em outras palavras, os indivíduos que participam da pesquisa clínica não devem ser submetidos a condições ou procedimentos anti-éticos ou desumanos, e isso inclui a proteção a sua privacidade. Um terceiro princípio poderia ser afirmado da seguinte forma: a pesquisa clínica deve aderir a uma metodologia rígida, a fim de assegurar a maior probabilidade possível de confiabilidade dos resultados. Metodologia e relatórios devem ser suficientemente claros para que esses resultados possam ser totalmente reprodutíveis por terceiros. Isso garantirá que os recursos gastos na pesquisa não sejam desperdiçados em esforços inúteis.</p>

<p>Isso nos leva a um conjunto de três princípios que podemos resumir como se segue:</p>

<ol>
<li>Defender o interesse do público.</li>

<li>Proteger a integridade do participante.</li>

<li>Seguir um método rígido e reprodutível.</li>
</ol>

<p>Qual é a melhor maneira de garantir que toda pesquisa clínica adote esses princípios? A resposta é muito difícil, e ninguém tem uma solução definitiva. Uma série de razões complica a perspectiva da pesquisa clínica, mas os fatores mais importantes parecem ser conflitos de interesse de financiadores e cientistas. No entanto, não se pode subestimar a influência das crenças culturais dos cientistas e do público em geral e uma falta de familiaridade com o significado e as deficiências dos métodos estatísticos. Estes não são problemas pequenos. O efeito destes problemas da pesquisa clínica é baixa probabilidade de publicação de resultados finais de ensaios clínicos (mesmo quando eles são clinicamente significativos) e falta de transparência de informações derivadas de ensaios clínicos.</p>

<p>Como reverter esta situação? Existem muitas iniciativas e proposições que tentam solucionar esse dilema. Resumidamente, podemos dividi-los em dois grandes conjuntos ou grupos: modelos de reforma fracos (conservadores) ou modelos de reforma fortes (não conservadores). Ambos pretendem o mesmo: otimizar a cadeia produtiva da informação na pesquisa clínica. A informação pode ser vista como um bem, o mais importante neste modelo. O problema pode ser declarado como uma utilização ineficiente deste bem. Os modelos conservadores geralmente não propõem mudanças importantes na forma como a informação é gerada e monetizada, mas tentam incutir uma disciplina sobre os agentes dessa cadeia de produção. Pode-se ter a propriedade da informação, mas será necessário cumprir uma série de boas práticas. Essas práticas incluem principalmente registrar a pesquisa clínica (ensaio) e seus resultados. Os modelos não conservadores, pelo contrário, desafiam o paradigma da geração de informação, sua monetização e propriedade. Baseiam-se na ideia de que a informação é propriedade de todas as pessoas. Esses modelos argumentam contra a propriedade da informação por indivíduos ou coletivos e defendem o uso generalizado da publicação aberta de informações na rede mundial.</p>

<p>Um bom exemplo de iniciativas conservadoras são a legislação de muitos países  especificamente solicitando o registro de todos e quaisquer ensaios clínicos. O Brasil é um exemplo desse tipo de regulamentação. Antes da aprovação de qualquer pesquisa clínica por um Comitê de Ética em Pesquisa, o projeto deve ser incluído na Plataforma Brasil, um registro centralizado para qualquer projeto de pesquisa clínica ou pré-clínica no país, mantido pelo Ministério da Saúde. No entanto, há pouca (se existe) fiscalização da lei no Brasil, e uma grande quantidade de projetos de pesquisa clínica que são registrados acaba por nunca informar se foram concluído ou tiveram resultados. Isso está longe de ser o ideal. Uma iniciativa internacional e independente que pode ser agrupada no nível conservador é <a href="http://alltrials.net">AllTrials.net</a> de Ben Goldacre, uma organização não governamental de grupos editoriais de ciência e muitos outros dedicados a difundir a mensagem: "todos os ensaios registrados, todos os resultados publicados". O ponto comum dos modelos de tipo conservador é que o eixo formado pelas empresas farmacêuticas, empresas editoriais científicas e instituições de renome com atividades de pesquisa vinculadas a empresas nem sequer é mencionado, e muito menos questionado.</p>

<p>Não obstante, praticamente todas as publicações científicas "de primeira linha" são produzidas nesta corrente produtiva de informação convencional. Elas são financiadas por grandes empresas farmacêuticas, geradas por instituições selecionadas e publicadas nas revistas científicas mais tradicionais e exclusivas. Como medida da influência deste eixo, podemos examinar a distribuição do Fator de Impacto da Thomson Reuters. Menos de 0,1% dos artigos publicados em 2014 foram altamente citados, fato que molda a distribuição de fatores de impacto e, conseqüentemente, o ranking dos periódicos mais prestigiados. Esse sistema de alça de retroalimentação positiva altamente redundante concentra a maioria das pesquisas clínicas importantes. Como uma conclusão lógica, pode-se afirmar que a maioria dos problemas com a transparência da ciência clínica surgem neste eixo.</p>

<p>Essa percepção alimentou iniciativas não conservadoras que visam subverter essa estrutura central da pesquisa clínica. Nos últimos anos, programas independentes e vagamente relacionados entre si se uniram no conceito de Ciência Aberta. A conceituação desta evoluiu de uma analogia aos softwares de código aberto a uma idéia mais complexa baseada principalmente na transparência da informação.</p>

<h2 id="oquecinciaaberta">O que é Ciência Aberta?</h2>

<p><img src="https://s3-eu-west-1.amazonaws.com/pfigshare-u-previews/2250225/preview.jpg" alt="taxonomia" /></p>

<p>De acordo com a página da iniciativa Foster (<a href="https://www.fosteropenscience.eu">Facilitate Open Science Training for European Research</a>), a ciência aberta é <code>um termo guarda-chuva que envolve vários movimentos visando remover as barreiras para compartilhar qualquer tipo de saída, recursos, métodos ou ferramentas, em qualquer fase do processo de pesquisa</code> (<a href="https://dx.doi.org/10.6084/m9.figshare.1508606.v3">Knoth, 2015</a>). Esse conceito é aberto e evolutivo, e agrega o compartilhamento gratuito da produção científica como um todo, abrangendo todas as suas múltiplas fases. Da bancada para as aplicações finais. Vai muito além do que as iniciativas conservadoras conceitualmente mais estreitas. Embora a maior parte do foco permaneça na publicação, Dados Abertos e Acesso Aberto, o sistema é muito mais do que isso.</p>

<p>A <a href="http://OECD.org">OCDE (Organização para Cooperação e Desenvolvimento Econômico)</a> afirma que a Open Science constitui na <code>produção dos principais resultados de pesquisa financiados publicamente - publicações e dados brutos de pesquisa - acessíveis ao público em formato digital sem restrição mínima</code> (<a href="http://dx.doi.org/10.1787/5jrs2f963zs1-en">OECD, 2015</a>). Não obstante, seu alcance é muito mais amplo e se estende para todo o ciclo de produção científica (<a href="https://www.fosteropenscience.eu/content/what-open-science-introduction">Fuente, 2016</a>).</p>

<p><img src="https://www.fosteropenscience.eu/sites/default/files/images/OpenScienceResearchInitiative-ResearchLifecycle.png" alt="ciclo" /></p>

<p>O conceito básico de Ciência Aberta é o de divulgar o conhecimento científico. Na verdade, essa idéia tem raízes que vem desde a Biblioteca de Alexandria, cujo objetivo principal era coletar todo o conhecimento do mundo na época, sob o patrocínio do governante do Egito Macedônio. Embora o início da ciência pública como um quadro conceitual tenha sido tradicionalmente atribuído ao nascimento de revistas acadêmicas no século XVII, a idéia de disseminar conhecimento é claramente de origem muito antiga na tradição ocidental. Um dos seus predecessores pode ter sido Aristóteles no Liceu de Atenas no século IV aC. Ele introduziu a noção de pesquisa cooperativa e coleta sistemática de observações empíricas (Lindberg, 1992).</p>

<p>Não obstante o avanço trazido pela invenção da imprensa no século XV e as revistas acadêmicas no século XVII, apenas nos anos 40 o sociólogo Robert King Merton definiu claramente a noção de propriedade comum das descobertas científicas. Na sua opinião, as realizações científicas são um produto de colaboração social e devem ser atribuídas à comunidade. O sistema de publicações acadêmicas baseadas em subscrições que tem sido o padrão surgiu  mais recentemente. O desenvolvimento de ferramentas digitais de tecnologia da informação e da comunicação introduziu um novo elemento disruptivo no ciclo produtivo científico. Em um ensaio de 2009 (<a href="http://www.nybooks.com/articles/2009/08/13/the-news-about-the-internet/">Massing, 2009</a>), o jornalista Michael Massing referiu-se à analogia entre a introdução da imprensa e o advento da internet (atribuída à Clay Shirsky da NYU):</p>

<blockquote>
  <p>A analogia histórica pode ser levada um passo adiante: assim como o advento da impressão ajudou a quebrar a influência da Igreja medieval sobre o fluxo de informações, a Internet afrouxou o controle dos meios de comunicação pelas empresas corporativas. Está ocorrendo um processo profundo e incontrolável de descentralização e democratização.</p>
</blockquote>

<p>O termo <strong>Ciência Aberta</strong> seria introduzido em 2003 pelo economista Paul David, tentando analisar a relação entre a produção científica pelo setor público e o aumento dos direitos de propriedade intelectual dos ativos de informação. Apesar de ainda ser um conceito vagamente definido com várias interpretações, o movimento da Ciência Aberta ganhou impulso em todo o mundo, e os decisores políticos e os pesquisadores estão discutindo ativamente sobre isso.</p>

<h2 id="esobrecinciaclnicaaberta">E sobre ciência clínica aberta?</h2>

<p>O produto lógico da aplicação direta de princípios científicos abertos à pesquisa clínica seria algo como "ciência clínica aberta". Existe alguma coisa como ciência aberta na pesquisa clínica? Na verdade, existe.</p>

<p>O cardiologista <a href="https://www.forbes.com/forbes/2010/0927/opinions-harlan-krumholz-yale-medicine-ideas-opinions.html">Harlan Krumholz</a>, mais conhecido por sua cruzada sem fim para a qualidade do atendimento clínico e pela pesquisa orientada para o paciente que remodelaram a medicina moderna, é provavelmente um dos pioneiros da ciência aberta na pesquisa clínica ou o que realmente poderíamos chamar de <strong>ciência clínica aberta</strong>. Estudando dados não publicados obtidos a partir de litígios, ele pôde mostrar que a Merck tinha dados que comprovavam que o Vioxx (rofecoxib) aumentava as mortes cardiovasculares antes de sua retirada do mercado. Casos como esse levaram ele e muitos outros que o seguiram a propor a divulgação de todos os dados clínicos em bancos de dados públicos, bem como o princípio central da ciência aberta. O Dr. Krumholz apresentou um conjunto de regras ("etapas", como ele escreveu) que constituem o mínimo necessário para 'trazer compartilhamento de dados e abrir a ciência na corrente principal da pesquisa clínica' (<a href="https://doi.org/10.1161/CIRCOUTCOMES.112.965848">Krumholz, 2012</a>):</p>

<ol>
<li>Publicar, em domínio público, o protocolo de estudo para cada ensaio clínico. O protocolo deve ser abrangente e incluir políticas e procedimentos relevantes para as ações tomadas no ensaio.</li>

<li>Desenvolver mecanismos para aqueles que possuem dados de avaliação para compartilhar seus dados brutos e dados individuais do paciente.</li>

<li>Incentivar a indústria a comprometer-se a colocar todos os seus dados de pesquisa clínica relevantes para produtos aprovados no domínio público. Esta ação reconheceria que o privilégio de vender produtos é acompanhado de uma responsabilidade de compartilhar todos os dados de pesquisa clínica relevantes para os benefícios e danos dos produtos.</li>

<li>Desenvolver uma cultura dentro da academia que valorize o compartilhamento de dados e a ciência aberta. Após um período em que os investigadores originais podem concluir seus estudos financiados, os dados devem ser desidentificados e disponibilizados para os pesquisadores em todo o mundo.</li>

<li>Identificar, dentro de todas as revisões sistemáticas, ensaios que não sejam publicados, usando fontes como <code>clinicaltrials.gov</code> e postagens reguladoras para determinar o que falta.</li>

<li>Compartilhar dados.</li>
</ol>

<p>A importância de compartilhar todos os dados clínicos não pode ser minimizada. Dados (<a href="https://doi.org/10.1136/bmj.i637">Chen, 2016</a>) mostram que menos da metade de todos os ensaios clínicos são publicados até 2 anos após sua conclusão, e isso inclui aqueles com financiamento público. Os ensaios com resultados negativos são menos prováveis ​​de serem publicados também. Um estudo demonstrou que quase todas as meta-análises que examinaram teriam suas conclusões afetadas por dados não publicados. Mesmo que o foco atual seja em padrões de qualidade metodológicos elevados para ensaios clínicos, nenhum método elegante pode compensar dados faltantes, como afirma o Dr. Krumholz. Uma revisão sistemática recente (<a href="https://doi.org/10.1136/bmj.h5002">Fleetcroft, 2015</a>) de insuficiência cardíaca foi prejudicada por causa da má divulgação e não divulgação de dados, embora os estudos clínicos fundamentais já tenham sido feitos. Uma tentativa de revisão (<a href="https://doi.org/10.1001/jama.2014.9646">Ebrahim, 2014</a>) de reanálises de ensaios controlados randomizados levou à conclusão perturbadora de que eles são extremamente raros e metodologicamente falhos, sugerindo que os resultados dos ensaios originais poderiam levar a conclusões diferentes. A ciência é baseada na própria possibilidade de teste de hipóteses, mas isso não parece ser a regra na ciência clínica.</p>

<p>Uma pesquisa (<a href="https://doi.org/10.1136/bmj.e7570">Rathi, 2012</a>) entre ensaístas clínicos demonstrou um forte suporte para compartilhar dados desidentificados. No entanto, a comunidade levantou uma série de preocupações: como garantir o uso adequado dos dados, a proteção dos interesses dos investigadores ou dos financiadores e a proteção dos participantes da pesquisa. Isso sugere que a comunidade acadêmica esteja pronta para a ciência clínica aberta, mas exige uma plataforma adequada para levá-la adiante com segurança para todas as partes envolvidas. Tal plataforma já pode ter nascido no projeto YODA da Universidade de Yale, que publicou (<a href="https://doi.org/10.7326/0003-4819-158-12-201306180-00009">Krumholz, 2013</a>) seus primeiros relatórios completos de dados clínicos abertos ao nível do paciente em 2013. A política institucional de compartilhamento de dados ao nível de pacientes mais antiga pode ser a do Instituto Nacional do Coração, Pulmão e Sangue (NHLBI) (<a href="https://doi.org/10.1186/1745-6215-14-201">Coady, 2013</a>), cujo repositório possui dados sobre mais de 560 mil participantes de 100 ensaios clínicos e estudos observacionais realizados desde 1989. Sua política evoluiu com o tempo e estabeleceu uma série de limitações sobre o compartilhamento e a utilização dos dados do paciente, tornando-o diferente de um acesso aberto. Muitos duvidam, no entanto, que a verdadeira ciência aberta é viável na ciência clínica (<a href="https://doi.org/10.1093/ehjqcco/qcv019">Flather, 2015</a>).</p>

<h2 id="concluso">Conclusão</h2>

<p>Não obstante os críticos e céticos, fica bem evidente que precisamos de uma melhor ciência clínica para orientar as decisões clínicas na cabeceira do leito do paciente. Não é uma questão de viabilidade, é mais uma questão de quando vamos implementar a ciência clínica aberta de uma vez.</p>

<h2 id="links">Links:</h2>

<ul>
<li><a href="https://en.m.wikipedia.org/wiki/Clinical_research">Clinical research, Wikipedia</a></li>

<li><a href="http://alltrials.net">AllTrials.net</a></li>

<li><a href="https://www.fosteropenscience.eu">Facilitate Open Science Training for European Research, FOSTER</a></li>

<li><a href="http://OECD.org">Organization for Economic Cooperation and Development, OECD</a></li>

<li><a href="https://www.forbes.com/forbes/2010/0927/opinions-harlan-krumholz-yale-medicine-ideas-opinions.html">Harper, Matthew. The most powerful doctor you never heard of. Forbes, 09/09/2010</a></li>
</ul>

<h2 id="references">References:</h2>

<ol>
<li><a href="https://dx.doi.org/10.6084/m9.figshare.1508606.v3">Knoth, Petr; Pontika, Nancy (2015): Open Science Taxonomy. figshare.</a></li>

<li><a href="http://dx.doi.org/10.1787/5jrs2f963zs1-en">OECD (2015), “Making Open Science a Reality”, OECD Science, Technology and Industry Policy Papers, No. 25, OECD Publishing, Paris</a></li>

<li>Gemma Bueno de la Fuente. What is open science? Introduction. <a href="https://www.fosteropenscience.eu/content/what-open-science-introduction">webpage</a>, retrieved in 10/15/2016.</li>

<li>Promoting openness at different stages of the research process (figure). Open Science and Research Initiative (2014). Open Science and Research Handbook. [English version] Available at <a href="https://avointiede.fi/documents/14273/0/Open+Science+and+Research+Handbook+v.1.0/50316d5d-440b-4496-b039-2997663afff8">webpage</a></li>
